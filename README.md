Personal Helm charts published via Gitlab pages

# How to use charts

helm repo add lam-bo-infra https://lam-bo-infra.gitlab.io/helm-charts

# References

https://tobiasmaier.info/posts/2018/03/13/hosting-helm-repo-on-gitlab-pages.html